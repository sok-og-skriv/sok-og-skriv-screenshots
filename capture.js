const puppeteer = require("puppeteer");
const xmlUrls = require('xml-urls')
const filenamifyUrl = require('filenamify-url');

let browser
var site = 'https://sok-og-skriv.now.sh/sitemap.xml'

const screenshot = async (x) => {  
  const page = await browser.newPage();
  const url = filenamifyUrl(x, {replacement: '__'});
  const file = `./public/images/${url}.png`
  
  await page.setViewport({
    width: 1024,
    height: 1080
  });

  await page.goto(x, {
    waitUntil: "networkidle0",
    timeout: 0
  });

  await page.screenshot({
    path: file,
    type: 'png',
    fullPage: true
  });

  await page.goto('about:blank') 
  await page.close() 
};

const getAllScreenshots = async () => {
  const links = ( async () => {
    const links = await xmlUrls(site, [])
    console.log(`Fant ${links.length} sider i sitemap.xml`);
    console.log(links);
    return links
  })();

  browser = await puppeteer.launch({
    headless     : true
  })

  const pages = await links
  const promises = await pages.map((x) => screenshot(x))
  await Promise.all(promises)
  await browser.close()

  console.log('DONE')
}

getAllScreenshots()
